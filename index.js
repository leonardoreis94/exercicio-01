const express = require('express');
const app = express();
const port = 8000;

//habilitar servidor
app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

//Get - Clientes
//1 - Parametro de envio via query (atributo nome)
app.get('/clientes',(req, res)=>{
    let dados = req.query;
    res.send(dados.nome);
});

//Post - Clientes
//1 - criar um parâmetro no header chamado access
//2 - parâmetros de envio via body
//3 - validar se o valor que foi enviado no header está correto para prosseguir com a execução
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.post('/clientes/acesso',(req, res)=>{
    let valores = req.headers;
    let informacoes = req.body;
    if (valores.access==123){
        res.send("Senha correta");
        console.log("Nome " + informacoes.nome);
        console.log("Idade " + informacoes.idade);
        console.log("Cep " + informacoes.cep);
    }else{  
        res.send("Senha incorreta");
    }
});

//Delete - Clientes
//1 - criar um parâmetro no header chamado access;
//2 - parâmetros de envio via param
//3 - validar se o valor que foi enviado no header está correto para prosseguir com a execução
app.delete('/clientes/delete/:codigoDeletar',(req, res)=>{
    let senhaDelete = req.headers;
    let informacoesDeletar = req.params.codigoDeletar;
    if (senhaDelete.access2==123){
        res.send("Senha correta");
        console.log("Codigo " + informacoesDeletar + " foi deletado do banco.")
    }else{  
        res.send("Senha incorreta");
    }
});

//Put - Clientes
//1 - criar um parâmetro no header chamado access;
//2 -  parâmetros de envio via body
//3 - validar se o valor que foi enviado no header está correto para prosseguir com a execução
app.put('/clientes/alterar',(req, res)=>{
    let senhaAlterar = req.headers;
    let atualização = req.body;
    if (senhaAlterar.access3==123){
        res.send("Senha correta");
        console.log("Codigo Cliente: " + atualização.codigoCliente);
        console.log("Nova atualização: " + atualização.informações);
    }else{  
        res.send("Senha incorreta");
    }
});

//Get - Funcionarios
//1 - Parametro de envio via query (atributo nome)
app.get('/funcionarios',(req, res)=>{
    let dados = req.query;
    res.send(dados.nome);
});

//Post - Funcionarios
//1 - criar um parâmetro no header chamado access
//2 - parâmetros de envio via body
//3 - validar se o valor que foi enviado no header está correto para prosseguir com a execução
app.post('/funcionarios/acesso',(req, res)=>{
    let valores = req.headers;
    let informacoes = req.body;
    if (valores.access==123){
        res.send("Senha correta");
        console.log("Nome " + informacoes.nome);
        console.log("Idade " + informacoes.idade);
        console.log("Cep " + informacoes.cep);
    }else{  
        res.send("Senha incorreta");
    }
});

//Delete - Funcionarios
//1 - criar um parâmetro no header chamado access;
//2 - parâmetros de envio via param
//3 - validar se o valor que foi enviado no header está correto para prosseguir com a execução
app.delete('/funcionarios/delete/:codigoDeletar',(req, res)=>{
    let senhaDelete = req.headers;
    let informacoesDeletar = req.params.codigoDeletar;
    if (senhaDelete.access2==123){
        res.send("Senha correta");
        console.log("Codigo " + informacoesDeletar + " foi deletado do banco.")
    }else{  
        res.send("Senha incorreta");
    }
});

//Put - Funcionarios
//1 - criar um parâmetro no header chamado access;
//2 -  parâmetros de envio via body
//3 - validar se o valor que foi enviado no header está correto para prosseguir com a execução
app.put('/funcionarios/alterar',(req, res)=>{
    let senhaAlterar = req.headers;
    let atualização = req.body;
    if (senhaAlterar.access3==123){
        res.send("Senha correta");
        console.log("Codigo Cliente: " + atualização.codigoCliente);
        console.log("Nova atualização: " + atualização.informações);
    }else{ 
        res.send("Senha incorreta");
    }
});